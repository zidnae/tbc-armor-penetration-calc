### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ 0f52f8be-4ca8-11eb-0361-2f44a5fd6e21

begin

    # This add s the Plots package to our environment, and imports it

    using Plots

    plotly()
	nothing
end

# ╔═╡ bc9dba04-42e4-11eb-3356-b7f7e7f2b09b
using Printf

# ╔═╡ d6f023fa-42e0-11eb-328b-9da41d7ea10e
begin
	function topct(v::Float64,figs::Int64=2)
		if figs == 2
		 @sprintf("%.2f",v*100) * "%"
		elseif figs == 3
			@sprintf("%.3f",v*100) * "%"
			elseif figs == 4
			@sprintf("%.4f",v*100) * "%"
			elseif figs == 5
			@sprintf("%.5f",v*100) * "%"
		end
	end
	nothing
end

# ╔═╡ c1337ff4-52e3-11eb-108d-bb9834752d68
md"""
# DRAFT: TBC Bear Theorycrafting Notes By Zidnae
These are incomplete working notes documenting TBC feral tanking mechanics. Expect to find errors in some of the below assumptions or calculations.
"""

# ╔═╡ c9fc6ee4-4c8e-11eb-3bc3-0353c0d0f086
md"""
# Feral Tank Ability Changes

## Maul[^8]
 - 10 rage cost with standard talents (15 base rage cost)
 - New rank grants +176 damage (up from +128 in Classic)
 - Threat generation was changed from a 1.75 damage multiplier to a static +threat component
   - **344 +threat** on hit
 - Idol of Brutality no longer reduces rage cost (instead increases base damage by 50)
 - No longer benefits from Savage Fury talent

``MaulDmg = (BasePaw + 176 + (\frac{attackPower}{14}) * 2.5) * Naturalist``


``MaulThreat = ((BasePaw + 176 + (\frac{attackPower}{14}) * 2.5) * Naturalist + 322) * (BearFormThreatMod + FeralInstinct)``

"""

# ╔═╡ 118d4294-4c92-11eb-05db-87f2be2c7f12
md"""
## Swipe[^9]
 - 15 rage cost with standard talents (20 base rage cost)
 - No longer has a 1.75 threat multiplier
 - New rank grants +84 base damage (up from +76 in Classic)
 - No longer benefits from Savage Fury talent
 - Idol of Brutality no longer reduces rage cost and instead increases base damage by 10
- **Applies 7% of your Attack Power to the total damage done**[^22]

``SwipeDmg = (84 + {attackPower}*.07 ) * Naturalist``


``SwipeThreat = (84 + {attackPower}*.07 ) * Naturalist) * (BearFormThreatMod + FeralInstinct)``

"""

# ╔═╡ 128ab02a-4c93-11eb-27cf-159b3b544034
md"""
## Mangle[^10]
 - New ability added in TBC
 - 15 rage cost with standard talents (20 base rage cost)
 - 6 second cooldown
 - Does 1.15x normal swing damage **plus** +155 bonus damage
 - Applies a debuff that increases damage from bleeds (from all sources no just the druid's bleeds) by 30%
 - Has a 1.3x threat multiplier

``MangleDmg = ((BasePaw + (\frac{attackPower}{14}) * 2.5) * 1.15 + 155 ) *  Naturalist``


``MangleThreat = ((BasePaw + (\frac{attackPower}{14}) * 2.5) * 1.15 + 155 ) *  Naturalist) * 1.3 * (BearFormThreatMod + FeralInstinct)``
 
"""

# ╔═╡ 307f1f80-4c95-11eb-13d6-bf2a802e5af1
md"""
## Lacerate[^11]
 - New ability added in TBC
 - 13 rage cost with standard talents (15 base rage cost)
 - Applies a stacking bleed DoT that does 155 damage total (31 damage per tick) over 15 seconds (one tick every 3 seconds)
   - The DoT can stack up to 5 times
   - The initial damage from the bleed gets applied immeadiately when the ability lands and affects targets who are immune to bleed (so the +threat componnent gets applied).
 - Lacerate has a +267 flat threat modifier applied on hit
 - Lacerate ticks **cannot** crit
 - Every 100 AP adds 1 damage per tick
 - Mangle buffs the bleed
 - Has a 50% damage-to-threat modifier[^16] meaning that a lacerate tick that did 100 damage would generate 50 threat (absent other threat modifiers)
- Lacerate applies the static threat component and debuff even when the attack is blocked. A blocked Lacerate can also proc Primal Fury.[^21]
- Lacerate damage ticks aren't reset when the ability is refreshed and will still tick on its usual 3 second timer.[^21]

``LacerateHitDmg = (31 + \frac{attackPower}{100})*  Naturalist``

``LacerateTickDmg = ((31 + \frac{attackPower}{100}) *  Naturalist) * NumStacks``


``LacerateInitialHitThreat = (((31 + \frac{attackPower}{100})*  Naturalist * 0.5) + 285 ) * (BearFormThreatMod + FeralInstinct)``


``LacerateTickThreat = (((31 + \frac{attackPower}{100}) *  Naturalist * 0.5) * NumStacks ) * (BearFormThreatMod + FeralInstinct)``
 
"""

# ╔═╡ 87a33612-52e8-11eb-2e1c-7786439c8b65
md"""
## Faerie Fire
- 610 armor shred
- 131 threat per application
"""

# ╔═╡ 241a2098-433d-11eb-0206-3929dacd9d11
md"""
## Barkskin
- The attack speed reduction component has been removed
- Barkskin is [still on the GCD](https://web.archive.org/web/20081017022333/https://blue.mmo-champion.com/2/9679404123-druid-tanks-swapped-out--no-bearwall.html) and cannot be cast via powershifting

## Frienzied Regeneration
- New rank added, each point of rage is converted into 25 hp (up from 20 hp in classic)
"""

# ╔═╡ b9aabf0a-52e2-11eb-1294-71e5ba92375c
begin
	naturalistmod = 1.1
	# 1.3 from bear form and an additonal .15 from talents
	bearformthreatmod = 1.45 
	BASE_PAW = 130
	bossarmordmgreduction = 1-.21# 21% damage reduction
	manglebleedbuff = 1.3
	BEAR_WEAPON_SPEED = 2.5
	md"""
	# Feral Tanking Rotation
	## Assumptions
	- Base average bear weapon damage is $(BASE_PAW)
	- Boss has 21% physical damage reduction from armor
	  - This is roughly what a TBC boss with 7700 armor will have with expose armor, faerie fire, and curse of recklessness up
	  - Lacerate is unaffected by boss armor since it is a bleed
	- Assuming 100% uptime on the mangle bleed debuff when calculating lacerate damage
	- Assuming 1.45 bear form threat modifier
	  - 1.3 from bear form and an additional .15 from talents
	- Assumes 5/5 in the Naturalist talent for the additional 10% physical damage
	- Assumes all normal hits (no critical hits or glancing blows)
	"""
end

# ╔═╡ d765a9fc-52d7-11eb-27eb-03a2322c841b
begin
	MANGLE_THREAT_MOD = 1.3
	MANGLE_BONUS_DAMAGE = 155
	MANGLE_DMG_MOD = 1.15
	function laceratetickdmg(ap;numstacks=5)
		(((31 + ap/100) * naturalistmod)* numstacks * manglebleedbuff )
	end
	function autodmg(ap;crit=false)
		dmg = (BASE_PAW + (ap/14) * 2.5) * naturalistmod * bossarmordmgreduction
		if crit
			return dmg * 2 * 1.1
		end
		return dmg
	end
	function laceratehitdmg(ap;crit=false)
		dmg = laceratetickdmg(ap,numstacks=1)
		if crit
			return dmg * 2 * 1.1
		end
		return dmg
	end
	function mangledmg(ap;crit=false)
		dmg = ((BASE_PAW + (ap/14) * 2.5) * MANGLE_DMG_MOD + MANGLE_BONUS_DAMAGE) * naturalistmod * bossarmordmgreduction
		if crit
			return dmg * 2 * 1.1
		end
		return dmg
	end
	function mauldmg(ap;crit=false)
		dmg = ((BASE_PAW + (ap/14) * 2.5) + 176) * naturalistmod * bossarmordmgreduction
		if crit
			return dmg * 2 * 1.1
		end
		return dmg
	end
	function swipedmg(ap;crit=false)
		dmg = (ap*.07 + 94) * naturalistmod * bossarmordmgreduction
		if crit
			return dmg * 2 * 1.1
		end
		return dmg
	end
	
	
	LACERATE_THREAT_MOD = .5
	function laceratetickthreat(ap;numstacks=5)
		(((31 + ap/100) * naturalistmod)* numstacks  * manglebleedbuff ) *bearformthreatmod * LACERATE_THREAT_MOD
	end
	LACERATE_STATIC_THREAT = 285
	function lacerate_initial_hit_threat(ap;crit=false)
		dmgThreat = (((31 + ap/100) * naturalistmod) * LACERATE_THREAT_MOD * manglebleedbuff) * bearformthreatmod
		if (crit)
			dmgThreat *= 2 * 1.1
		end
		staticThreat = LACERATE_STATIC_THREAT * bearformthreatmod
		return dmgThreat + staticThreat
	end
	function autothreat(ap;crit=false)
		return autodmg(ap,crit=crit) * bearformthreatmod
	end

	function manglethreat(ap;crit=false)
		dmgThreat = mangledmg(ap)*MANGLE_THREAT_MOD * bearformthreatmod
		if (crit)
			dmgThreat =dmgThreat*2 *1.1
		end
		return dmgThreat
	end
	MAUL_STATIC_THREAT = 344
	function maulthreat(ap;crit=false)
		dmgThreat = mauldmg(ap)*bearformthreatmod
		if (crit)
			dmgThreat = dmgThreat * 2 * 1.1
		end
		staticThreat = MAUL_STATIC_THREAT*bearformthreatmod
		return dmgThreat + staticThreat
	end
	function swipethreat(ap;crit=false)
		return swipedmg(ap,crit=crit) * bearformthreatmod
	end
	function lacerate_hit_dmg_and_x_ticks(ap;numticks=1,numstacks=5)
		laceratetickdmg(ap,numstacks=numstacks)*numticks + laceratetickdmg(ap,numstacks=1)
	end
	function lacerate_hit_threat_and_x_ticks(ap;numticks=1,numstacks=5,crit=false)
		laceratetickthreat(ap,numstacks=numstacks)*numticks + lacerate_initial_hit_threat(ap,crit=crit)
	end
	

	
	nothing

end

# ╔═╡ f5529f3c-52d8-11eb-1af7-236ecc764460
begin
		attackpowers = range(0,8000,step=10)
		plot(attackpowers,laceratetickdmg.(attackpowers),xlabel="Attack Power",ylabel="Damage",label="5 Stacks Lacerate Tick",title="Ability Damage vs Attack Power",legend = :outertopleft)
	plot!(attackpowers,autodmg.(attackpowers),label="Auto")
	plot!(attackpowers,mauldmg.(attackpowers),label="Maul")
		plot!(attackpowers,mangledmg.(attackpowers),label="Mangle")
	plot!(attackpowers,swipedmg.(attackpowers),label="Swipe")
	plot!(attackpowers,laceratehitdmg.(attackpowers),label="Lacerate Hit")

end

# ╔═╡ 4463e0ac-52e0-11eb-39a3-6fa91893272f
begin
		plot(attackpowers,laceratetickthreat.(attackpowers),xlabel="Attack Power",ylabel="Threat",label="5 Stacks Lacerate Tick",title="Ability Threat vs Attack Power",legend = :outertopleft)
	plot!(attackpowers,autothreat.(attackpowers),label="Auto")
		plot!(attackpowers,manglethreat.(attackpowers),label="Mangle")
	plot!(attackpowers,swipethreat.(attackpowers),label="Swipe")
	plot!(attackpowers,maulthreat.(attackpowers),label="Maul")
	plot!(attackpowers,lacerate_initial_hit_threat.(attackpowers),label="Lacerate Hit")

end

# ╔═╡ eca6341a-52e3-11eb-0f4e-e79ff972f5c0
md"""
Based on old Elitest Jerk threads[^15] the conventional wisdom for bear tanking was to:
- Use mangle on cooldown
- Use either lacerate or swipe while mangle is on cooldown as your rage dump
- Use auto-attacks to generate rage and to only cast maul if you have excessive rage

The above graphs corroborate this story. Mangle is a bear's best scaling and highest threat generating ability and should be used on cooldown. 

While maul is a great threat generating ability not only does it consume rage but it robs your auto-attack of the ability to generate rage so the effective rage cost is 30+ per maul. In a rage limted scenario using that rage for mangle, lacerate, and swipe would be more effective.

There seemed to have been some debate[^16] regarding wheather swipe or lacerate was better ability for single target tanking. Lacerate scales poorly due to its 50% damage to threat modifier and relies primarily on its static threat component to generate threat. Swipe on the other hand has the standard 100% threat modifier and therefore scales better.

"""

# ╔═╡ c7b4edf4-59d7-11eb-35cd-e5f991cd4571
begin
		# Computes the break even point between lacerate spam and swipe spam threat 
		function lacerate_swipe_breakeven_point(;crit=false)
		final_ap = 0
			for ap in attackpowers
				lacerate_threat = lacerate_hit_threat_and_x_ticks(ap,numticks=0,crit=crit)
				swipe_threat = swipethreat(ap,crit=crit)
				if swipe_threat > lacerate_threat
				final_ap = ap
					break
				end
			end
			return final_ap, swipedmg(final_ap,crit=crit)
		end
	lacerate_Swipe_breakpoint_ap, lacerate_Swipe_breakpoint_swipe_dmg = lacerate_swipe_breakeven_point()
	lacerate_Swipe_breakpoint_swipe_dmg = round(lacerate_Swipe_breakpoint_swipe_dmg)
		crit_lacerate_Swipe_breakpoint_ap, crit_lacerate_Swipe_breakpoint_swipe_dmg = lacerate_swipe_breakeven_point(crit=true)
	md"""
	Based on the below plots, at lower gear levels mange + lacerate spam is the optimal threat generating rotation for single target tanking. With better gear swipe eventually scales up to the point where it becomes worthwhile to start weaving in swipes and letting lacerate tick two or three times before refreshing. 
	
	
	This point is the intersection between `Swipe` and `Lacerate Hit` in the plots below. Calculating this point, **when swipe hits start doing more than $lacerate_Swipe_breakpoint_swipe_dmg damage *Mangle + Lacerate + Swipe* is better threat generation than just *Mangle + Lacerate* spam**. This point should occur around $crit_lacerate_Swipe_breakpoint_ap attack power for critical hits and $lacerate_Swipe_breakpoint_ap attack power for non-crits. The exact break even point is dependent on a player's critical hit rate and other factors like the exact enemy boss armor.
	

	"""
end

# ╔═╡ 37dfaf28-52e5-11eb-2614-8725f5ba4bbb
begin
		plot(attackpowers,swipethreat.(attackpowers),xlabel="Attack Power",ylabel="Threat",label="Swipe",title="Swipe vs Lacerate Threat",legend = :outertopleft) 
		plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=0),label="Lacerate Hit")
	plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=1),label="Lacerate Hit + One Tick")
	plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=2),label="Lacerate Hit + Two Ticks")
	plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=3),label="Lacerate Hit + Three Ticks")
		plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=4),label="Lacerate Hit + Four Ticks")

end

# ╔═╡ bdb8e66e-579f-11eb-1461-a17d261169a5
begin
		plot(attackpowers,swipethreat.(attackpowers,crit=true),xlabel="Attack Power",ylabel="Threat",label="Swipe",title="Swipe Crit vs Lacerate Crit Threat",legend = :outertopleft) 
		plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=0,crit=true),label="Lacerate Hit")
	plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=1,crit=true),label="Lacerate Hit + One Tick")
	plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=2,crit=true),label="Lacerate Hit + Two Ticks")
	plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=3,crit=true),label="Lacerate Hit + Three Ticks")
		plot!(attackpowers,lacerate_hit_threat_and_x_ticks.(attackpowers,numticks=4,crit=true),label="Lacerate Hit + Four Ticks")
end

# ╔═╡ 45b83ac4-64e3-11eb-02e3-ffddc00ff07b
md"""
*Note: For the crit comparison plot the damage values of swipe and the lacerate hit are scaled up by 10% due to the predatory instincts talent*
"""

# ╔═╡ 80f48a1a-553d-11eb-1913-1d1dce727bd0
md"""
# Rage Generation[^19]

## Rage From Dealing Damage
The rage generation formula from dealing damage was updated in TBC

**Vanilla Formula**
```
Rage = (Damage Dealt) / (Rage Conversion) * 7.5
```

**TBC Formula**
```
Rage = ((Damage Dealt) / (Rage Conversion) * 7.5 + (Weapon Speed * Factor))/2
```

where
```
Rage Conversion at level 60: 230.6
Rage Conversion at level 70: 274.7

For Dealing Damage:
Main Hand Normal Hits: Factor=3.5
Main Hand Crits: Factor=7
Off Hand Normal Hits: Factor=1.75
Off Hand Crits: Factor=3.5
```

Reading between the lines the intention of the rage generation changes appears to have been to reduce rage generation's scaling by halving rage generation from damage dealt. To partially offset this change the devs added a static rage generation term to each attack with the static component being larger for main hand hits over off-hand hits to reduce the amount of rage off-hand melee hits generate.


Summarizing using a bear's 2.5 weapon speed, **the static rage term will cause each landed hit generates an additional flat +3.125 rage in addition to the rage from damage dealt. The newly added halving term should significantly decrease the amount of rage generated per hit from damage relative to vanilla.** Though that's also dependent on well bears will scale relative to the updated rage conversion factor for level 70's. Critical hits will generate double the amount of rage including the static portion.


TODO: Verify that rage generation is normalized to 2.5 and not caster form weapon

The talent *Primal Fury* also causes a bear to generate 5 rage whenever they land a critical strike from both auto attacks and abilities like mangle, swipe, and lacerate.




"""

# ╔═╡ 0e6409ca-553e-11eb-0f81-2301274e5ad9
begin
	RAGE_CONVERSION = 274.7
	MAIN_HAND_FACTOR = 3.5
	function rage_generated_from_dealing_damage(dmg)
		(dmg / RAGE_CONVERSION * 7.5 + (BEAR_WEAPON_SPEED * MAIN_HAND_FACTOR))/2 
	end
	
		plot(attackpowers,rage_generated_from_dealing_damage.(autodmg.(attackpowers)),xlabel="Attack Power",ylabel="Rage",label="Rage Generated",title="Avg Rage Generated by Auto Attack Normal Hit",legend = :outertopleft) 
end

# ╔═╡ eeee1aca-553f-11eb-0972-23722e09b627
md"""
## Rage From Damage Taken

The formula for rage generated from taking damage remains unchanged from vanilla
```
Rage Gained = (Damage Taken) / (Rage Conversion at Your Level) * 2.5
```
"""

# ╔═╡ cc6d38ac-4239-11eb-1091-cf1f1fe191e5
begin
	expertise_softcap =  @sprintf("%.2f",(6.5 / (.25/3.9425)))
	expertise_hardcap =  @sprintf("%.2f",(14 / (.25/3.9425)))
	md"""
	# Player Attack Table
	
	## Expertise[^25]
	### Expertise Rating vs Expertise Points[^3]
	3.9425 expertise rating provides 1 expertise point
	
	Each expertise point reduces your target's chance to dodge or parry by .25%
	
	**Therefore each point of expertise rating reduces your target's chance to dodge or parry by $(topct(.0025/3.9425,3))**
	
	This means as a DPS standing behind the boss each point of expertise increases the chance for you attacks to land by $(topct(.0025/3.9425,3)) as it only reduces your chance to miss. 
	
	Whereas for a tank standing in front of the boss each point of expertise rating increases the chance for you attacks to land by $(topct(.0025*2/3.9425,3))
	
	Note that the conversion from rating to skill is done in whole skill points, meaning that any fractional part is rounded down; e.g. if you have 39 expertise rating which translates to ~9.89270 expertise skill, you will effectively gain only 9 expertise skill from it.
	
	### Expertise Caps
	
	The dodge chance of a mob is calculated as follows:[^4]
	```
	    DodgeChance = 5% + (TargetLevel*5 - AttackerSkill) * 0.1%
	```
	So for a level 70 player with the baseline 350 attack skill and a level 73 mob with a 365 defense skill the **mob will have a base 6.5% chance to dodge the attack**.
	
	**The parry chance of a mob 3 levels higher than a player is 14%**[^5]
	
	##### Expertise Soft Cap
	A level 70 player would need an **expertise rating of $(expertise_softcap) to remove target dodges from the attack table.**
	
	As a DPS standing behind the boss who cannot get parried $(expertise_softcap) expertise rating would be hardcaped at this value.
	
	Up until the expertise soft cap each point of expertise reduces the mobs chance to both dodge **and** parry. Any expertise over the soft cap only reduces your chance to be parried.
	
	##### Expertise Hard Cap
	A tank would need expertise rating of $(expertise_hardcap) to become unparryable. 
	"""
end

# ╔═╡ 6e2dfa2e-4239-11eb-3386-15e88e1eec08
begin
	hit_cap =  @sprintf("%.2f",(7 + (365 - 350 - 10)*0.4) / (1/15.77))
	md"""
	
	## Melee Hit[^6]
	15.77 hit rating reduces your chance to miss by 1% at level 70.
	
	Therefore **each point of hit rating reduces your chance to miss by $(topct(.01/15.77,3))**.
	
	A player's chance to miss is calculated as follows
	
	```
	If the difference between the mob's Defense Skill and your Weapon Skill is less than or equal to 10:
	    MissChance = 5% + (Defense Skill - Weapon Skill)*0.1%
	```
	
	```
	If the difference between the mob's Defense Skill and your Weapon Skill is greater than 10:
	    MissChance = 7% + (Defense Skill - Weapon Skill - 10)*0.4%
	```
	
	A level 70 player's miss chance with 350 weapon skill againist various level mobs is summarized below:

	| Level | Dodge Chance |
	|-------|-----------|
	| 70    | 5.0%    |
	| 71    | 5.5%    |
	| 72    | 6.0%    |
	| 73    | 9.0%    | 

	 
	##### Hit Hard Cap
	If you are a Level 70 character with a weapon Skill of 350, you need a **hit Rating of $(hit_cap) to remove misses from your attack table**.
	
	##### Taunts
	Midway through TBC both of the druid's taunts (Growl and Challanging Roar) were changed to use melee hit instead of spell hit to reduce resist chance. 
	
	They both have still have a spell's base 17% chance to resist however. Assuming that the last 1% miss chance cannot be reduced via hit a player would need 16% hit or 252.32 hit rating to reduce taunt resist chance to 1%.
	"""
end

# ╔═╡ a9c7cc84-5bf4-11eb-31fa-b93360c96fbf
md"""

## Block[^26]

Unchanged from vanilla, **bosses have a 5% chance block a player** attacking from the front. The full formula is 

```
If the target is a mob:
    BlockChance = MIN(5%, 5% + (TargetLevel*5 - AttackerSkill) * 0.1%)

If the target is a player:
    BlockChance = PlayerBlock + (PlayerDefense - AttackerSkill) * 0.04%
```

Mob block chance in table form:

ΔLevel|ΔSkill|Block chance
:---:|:---:|:---:
0|0|5.0%
1|5|5.0%
2|10|5.0%
3|15|5.0%

"""

# ╔═╡ d096422a-5300-11eb-3be2-07a012305a34
md"""
## Glancing Blows[^17][^27]

### Glance Chance
```
GlanceChance = MAX(0, 6% + (TargetLevel*5 - AttackerSkill) * 1.2%)
```

### Glance damage penalty

The damage penalty is determined with a random roll in a range calculated as follows:

```
SkillDiff = TargetLevel*5 - AttackerSkill

If defense minus weapon skill is 11 or more:
    LowEnd = MAX(0.01, MIN(1.4 - 0.05 * SkillDiff, 0.91))
    HighEnd = MAX(0.2, MIN(1.3 - 0.03 * SkillDiff, 0.99))

If defense minus weapon skill is 10 or less (this is the old Beaza formula):
    LowEnd = MAX(0.01, MIN(1.3 - 0.05 * SkillDiff, 0.91))
    HighEnd = MAX(0.2, MIN(1.2 - 0.03 * SkillDiff, 0.99))
```

It then averages out over multiple hits to the value:

```
GlanceAveragePenalty = (LowEnd + HighEnd) / 2
```

And in table form:

ΔLevel|ΔSkill|Glance chance|Glance penalty (avg)
:---:|:---:|:---:|:---:
0|0|6.0%|5.0%
1|5|12.0%|5.0%
2|10|18.0%|15.0%
3|15|24.0%|25.0%

As with vanilla, yellow hits (mangle, swipe, maul, lacerate) cannot glance.

"""

# ╔═╡ 3d4093e0-4cb6-11eb-38a1-e51d199defdf
md"""
## Crit Rating
At level 70:
- 22.1 crit rating equals 1% crit
- 25 Agility equals 1% crit

### Crit Cap[^20]
To recap up to this point the attack table for various level mobs is:


ΔLevel|Miss%|HitCap%|Dodge%|Parry%|Glance%|GlanceDmg|Block%|CritSupp%
:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:
0|5.0%|5.0%|5.0%|5.0%|6%|5%|5%|0.0%
1|5.5%|5.5%|5.5%|5.5%|12%|5%|5%|-1.0%
2|6.0%|6.0%|6.0%|6.0%|18%|15%|5%|-2.0%
3|8.0%|9.0%|6.5%|14.0%|24%|25%|5%|-4.8%

The maximum amount of crit a player can accrue before every white hit becomes a crit is calculated as follows:


``
\begin{equation}
\small Crit_{cap}\ =\ 100-P\left(Miss\right)-P\left(Dodge\right)-P\left(Block\right)-P\left(Parry\right)-P\left(Glance\right)+Crit_{\sup}
\end{equation}
``


where ``Crit_{\sup}`` is the crit supression value which is typically 4.8% when fighting a level 73 mob.

A player's crit cap can be increased by decreasing their chance to miss, dodge, and parry via expertise and hit rating. Yellow hits follow the same forumla but cannot glance.

### Crit Supression[^28]

The crit supression formula matches the [Beaza formula](https://wowwiki-archive.fandom.com/wiki/Critical_strike?diff=prev&oldid=349108)

```
If the target is a mob and weapon minus defense skill is less than 0:
    CritChance = AttackerCrit + (AttackerSkill - TargetLevel*5) * 0.2%

If the target is a mob and weapon minus defense skill is above or equal to 0:
    CritChance = AttackerCrit + (AttackerSkill - TargetLevel*5) * 0.04%

If the target is a player:
    CritChance = AttackerCrit + (AttackerSkill - TargetDefense) * 0.04%
```

Skill-diff-based crit modifier in table form:

ΔLevel|ΔSkill|Modifier
:---:|:---:|:---:
0|0|0.0%
1|5|-1.0%
2|10|-2.0%
3|15|-3.0%

Additionally, when fighting +3 level mobs there's a flat **-1.8%** suppression placed on your total crit chance gained from auras. Auras in this context are talents such as Cruelty or Axe Specialization, gear that gives crit rating through `Equip:` effects, buffs that increase you crit rating or chance like Songflower Serenade or Leader of the Pack, and consumables such as Elixir of the Mongoose. It does not include crit gained indirectly through Agility, neither base Agility nor Agility from gear.

If you don't have any crit from auras you are not affected by this, but still affected by the -3% modifier due to skill/defense difference described in the table above. Realistically, you will most likely have at least 2% crit from auras through one of the sources mentioned above even while leveling past level 10 (think 2/5 Cruelty) and certainly at level 70, putting your total crit modifier against +3 level mobs at **-4.8%**.
"""

# ╔═╡ 6b217a20-42ef-11eb-2068-21a3b2fdec3e
md"""
## Haste[^7]

At level 70, each 15.77 haste rating equates to 1% attack speed. 

Haste rating stacks additively with itself and then is converted into a percentage that stacks in a multiplicative manner with other sources of haste (like Bloodlust or Slice and Dice).

The formula for determining attack speed is:

``Speed=\frac{Base\ Weapon\ Speed}{\prod\limits_{i=1}^n \left(1+\frac{Haste_i}{100}\right)\left(1+\frac{\sum_{j=1}^m Haste\ Rating_j}{1577}\right)}``
"""

# ╔═╡ c93ebdd0-4ca8-11eb-00ce-03acafd0d177
begin 
	function attack_speed(haste_rating, haste_sources, base_speed)
		return base_speed/reduce(*,[1 + haste/100 for haste in haste_sources];init=(1 + haste_rating / 1577))
	end
	haste_ratings = range(0,1000,step=10)
	attack_speeds = attack_speed.(haste_ratings,[0.0],2.5)
	plot(haste_ratings,attack_speeds,xlabel="Haste Rating",ylabel="Attack Speed",title="Attack Speed vs Haste Rating",label="Default")
	attack_speeds_with_bloodlust = attack_speed.(haste_ratings,[35.0],2.5)
	plot!(haste_ratings,attack_speeds_with_bloodlust,label="With Bloodlust")
end

# ╔═╡ 38e99776-4228-11eb-33b6-0b0d9a13879a
md"""
## Armor Penetration[^1][^2]

Armor Penetration is a unique stat in that it is the only stat to scale with Increasing Returns as opposed to Diminishing Returns. More Armor Penetration yields more % increase in damage until the target's armor reaches 0.
 
The formula for damage reduction via armor for a mob over level 60 is:

``DR = \displaystyle\frac{Armor}{(Armor - 22167.5 + 467.5 * MobLevel)}``

Armor penetration calculator can be found below:

[WoW TBC Armor Penetration Calculator](https://zidnae.gitlab.io/tbc-armor-penetration-calc/)

"""

# ╔═╡ b4009394-432a-11eb-05ea-397eecfc9d15
begin

md"""
# Defensive Stats
## Armor[^12]

The formula for damage reduction from armor is:
```
DamageReduction% = Armor / (Armor + (467.5 * AttackerLevel - 22167.5))
```

Player armor cap for level 70-73 targets are as below

| Level | Armor Cap |
|-------|-----------|
| 70    | 31,672    |
| 71    | 33,075    |
| 72    | 34,477    |
| 73    | 35,880    | 

	
**Dire bear form grants a 400% bonus to base armor from items** but not armor gained from agility, set bonuses, buffs, enchants, etc. 

Each point of agility grants 2 armor but again this armor does not benefit from the dire bear form modifier.
	
"""

end

# ╔═╡ 9e9edbc0-4ca6-11eb-1fba-9d4e2c613d91
begin
	function damage_reduction_from_armor(armor, attacker_level)
		armor / (armor + (467.5 * attacker_level - 22167.5))
	end
	x = range(0,stop=35880,step=10)
	y = damage_reduction_from_armor.(x,73) .* 100
	plot(x,y,xlabel="Armor",ylabel="Damage Reduction %",title="Damage Reduction Againist Level 73 Attacker",formatter = identity,yticks=range(5,stop=75,step=10),xticks=range(0,stop=35880,step=5000),legend=false)
	
end

# ╔═╡ c048fdda-42f3-11eb-1ae6-b3e2f74aa3ed
begin
	crit_reduction_per_resiliance_rating =  .01 / 39.4
	md"""
	## Resilience Rating[^18]
	 **39.4 resilience rating reduces the chance to be hit by a critical hit by 1%**, reduces the damage you take from crits that land by 2%, reduces damage from DOTs by 1%, reduces damage from players by 1%, and reduces the amount of mana drained by mana burns/drains by 2% at level 70
	
	Therefore **each point of resilience rating reduces the chance to be critically hit by $(topct(crit_reduction_per_resiliance_rating,4))**
	
	A level 73 mob with 365 weapon skill has a **5.6% chance to land a critical hit on a player**. 
	
	The feral druid talent [Survival of the Fittest](https://tbc-twinhead.twinstar.cz/?spell=33853) reduces the chance a druid will be hit by a critical hit by 3%.
	
	The remaining 2.6% crit chance needs to be mitigated via resilience rating or defense rating.
	
	**103 resilience rating would be necesary to reduce the chance to be critically hit to zero** assuming the the level 70 player has the base 350 defense skill. 
	
	More realistically some combination of resilience rating and defense rating would be used to reduce the chance to be critucally hit to zero 
	"""
end

# ╔═╡ 764c10aa-4cad-11eb-19c2-592a64a7d875
begin
	avoidance_per_defense_rating = 1 / 2.3654 * .0004 * 2 
	crit_reduction_per_defense_rating =  1 / 2.3654 * .0004
	defense_rating_needed_for_uncritable = .026 / crit_reduction_per_defense_rating 
	defense_for_one_pct_avoidance = @sprintf("%.3f",0.01/avoidance_per_defense_rating)
	md"""
	
## Defense Rating
**2.3654 defense rating = 1 defense skill** at level 70
	
Each defense skill increases the player's dodge, block, parry, and chance to be missed by 0.04%. 
	
Additionally each defense skill reduces the chance to be critically hit by 0.04%.
	
Since bears can neither block nor parry **each point of defense rating increaes a druid's avoidance by $(topct(avoidance_per_defense_rating,4))**
	
And **each point of defense rating reduces the chance to be critically hit by $(topct(crit_reduction_per_defense_rating,4))**
	
**To reduce the chance to be critically hit to zero (assuming zero resilience rating) a druid would need $(defense_rating_needed_for_uncritable) defense rating**
	
Note that the conversion from rating to skill is done in whole skill points, meaning that any fractional part is rounded down; e.g. if you have 40 defense rating which translates to ~16.91045 defense skill, you will effectively gain only 16 defense skill from it.
	
## Uncrittability
A table summarizing respective defense and resilience combinations that achieve uncrittability is shown below
![Druid Uncrittable Table](https://cdn.discordapp.com/attachments/780185714942935091/855597677379321866/druid_def_res_v03.png)
	
# Crushing Blows


The formula for crushing blows is:
```
CrushingBlowChance = ([AttackerSkill - PlayerDefenseSkill] * 0.02) - 0.15
```
	
Having added defense beyond a player's level cap (i.e. by talents) does **NOT** reduce crush chance, however having defense skill less than the level cap increases crush chance.

Based on the above formula **a level 73 boss has a 15% chance to crush a player**.
	
## Uncrushability
A tank is uncrushable when a tanks total avoidance exceeds 102.4%. In other terms a tank is uncrushable when your combined Chance to be Missed, Dodge, Parry, and Block exceeds 102.4%.
	
In practice druid tanks will be unable to become uncrushable due to lacking parry and block, two important sources of avoidance. The conventional wisdom is that while druids can be crushed, unlike warrior and paladin tanks, they make up for it by having more armor and a larger health pool to soak the additional damage. 
	
"""
end

# ╔═╡ 8ba85768-4caf-11eb-3b46-079d676838e7
md"""
## Avoidance[^13]
For level 70 druids in TBC:

### Agility
**14.7059 Agility = 1% dodge**

### Dodge Rating
**18.923 dodge rating = 1% dodge** 

### Defense Rating
**$(defense_for_one_pct_avoidance) defense rating = 1% avoidance**

### Total Avoidance
``Avoidance = Base\ Dodge + \frac{Agility}{14.7059} + \frac{Dodge\ Rating}{18.923} + \frac{Defense\ Rating}{29.568} + Talent\ and\ Race\ contributions)``

"""

# ╔═╡ f14a269c-4cb3-11eb-379b-6952f9cd0650
begin
	stam_to_health_ratio = 10 * 1.25 * 1.20 * 1.1 * 1.03
	stam_to_health_ratio_tauren = 10 * 1.25 * 1.20 * 1.1 * 1.05 * 1.03
	str_to_ap_ratio = 2 * 1.03 * 1.1
	agi_to_dodge_ratio = 14.7059 / (1.1 * 1.03)
	agi_to_crit_ratio = 25 / (1.1 * 1.03)

	md"""
	## Base Attributes
	The feral druid talent [Survival of the Fittest](https://tbc-twinhead.twinstar.cz/?spell=33853) increases all stats by 3%.
	
	The paladin buff *Blessing of Kings* increases all stats by 10%.
	
	### Strength
	Base: 1 strength = 2 AP
	
	With Buffs: 1 strength = $(str_to_ap_ratio) AP
	
	### Agility
	Base:       14.7059 Agility = 1% dodge
	
	Base: 25 Agility = 1% crit
	
	With Buffs: $(agi_to_dodge_ratio) Agility = 1% dodge
	
	With Buffs: $(agi_to_crit_ratio) Agility = 1% crit
	
	### Stamina
	Base: 1 stamina = 10 hp
	
	Dire bear form grants a 25% stamina bonus.
	
	The Heart of the Wild talent grants another 20% bonus stamina while in bear form which stacks multiplicatively.
	
	The tauren ratio increases hp by 5% from all sources including stamina.
	
	Combined with the 10% *Kings* bonus and 3% *Survival of the Fittest* bonus we get the following stamina to health ratios:
	
	**Night Elf Stamina to Health: $(stam_to_health_ratio)**
	
	**Tauren Stamina to Health: $(stam_to_health_ratio_tauren)**
	"""
end

# ╔═╡ d8f4788e-42dd-11eb-0e86-f97e7de17923
md"""
[^1]: [Elitist Jerks TBC Armor Penetration Post](https://web.archive.org/web/20071202181137/http://elitistjerks.com/498690-post35.html)
[^2]: [Elitist Jerks TBC Fury Warrior Post](https://web.archive.org/web/20080913120556/http://elitistjerks.com/f31/t22705-dps_warrior_compendium/)
[^3]: [TBC Hit and Expertise](https://rrvs.blogspot.com/2008/02/hit-and-expertise.html)
[^4]: [Classic Warrior Attack Table Dodge](https://github.com/magey/classic-warrior/wiki/Attack-table#dodge)
[^5]: [Classic Warrior Attack Table Parry](https://github.com/magey/classic-warrior/wiki/Attack-table#parry)
[^6]: [WoW Wiki TBC Hit](https://wowwiki.fandom.com/wiki/Hit?oldid=1584399)
[^7]: [WoW Wiki TBC Haste](https://wowwiki.fandom.com/wiki/Haste)
[^8]: [Wowpedia TBC Maul](https://wow.gamepedia.com/Maul?oldid=1523996)
[^9]: [Wowpedia TBC Swipe](https://wow.gamepedia.com/Swipe_(Bear_Form)?oldid=1635045)
[^10]: [Wowpedia TBC Mangle](https://wow.gamepedia.com/Mangle?oldid=1524023)
[^11]: [Wowpedia TBC Lacerate](https://wow.gamepedia.com/Lacerate?oldid=1524028)
[^12]: [WoW Wiki TBC Armor](https://wowwiki.fandom.com/wiki/Armor?oldid=1435631)
[^13]: [WoW Wiki TBC Dodge](https://wowwiki.fandom.com/wiki/Dodge?oldid=1290773)
[^14]: [Elitest Jerks Avoidance Stacking](https://web.archive.org/web/20080730053612/http://elitistjerks.com/f31/t16211-tanking_attempting_reach_100_avoidance/)
[^15]: [Elitest Jerks Feral Druid Megathread](https://web.archive.org/web/20080913120406/http://elitistjerks.com/f31/t16902-feral_druid_megathread//)
[^16]: [Lacerate vs Swipe Elistest Jerks Threat](https://web.archive.org/web/20080930202005/http://elitistjerks.com/717764-post3217.html)
[^17]: [WoW Wiki TBC Glancing Blows](https://wowwiki.fandom.com/wiki/Glancing_blow?oldid=1499456)
[^18]: [WoW Wiki Resilience](https://wowwiki.fandom.com/wiki/Combat_rating_system?oldid=1566975)
[^19]: [The New Rage Formula](https://wowwiki.fandom.com/wiki/Rage)
[^20]: [Marrow’s Compendium of Dragonslaying](https://bookdown.org/marrowwar/marrow_compendium/mechanics.html#glancing_blows)
[^21]: [Druid Facts](https://web.archive.org/web/20080914151400/http://druid.wikispaces.com/Druid_Facts)
[^22]: [Druid Game Mechanics](https://web.archive.org/web/20080913173942/http://druid.wikispaces.com/Druid_Game_Mechanics)
[^23]: [Classic Warrior Attack Table Crit](https://github.com/magey/classic-warrior/wiki/Attack-table#critical-strike)
[^24]: [TBC Patch 2.4 Bear Footage](https://www.youtube.com/watch?v=P_UnxDlvwd4)
[^25]: [Magey TBC Expertise](https://github.com/magey/tbc-warrior/wiki/Attack-table#expertise)
[^26]: [Magey TBC Block](https://github.com/magey/tbc-warrior/wiki/Attack-table#block)
[^27]: [Magey TBC Glancing](https://github.com/magey/tbc-warrior/wiki/Attack-table#glance)
[^28]: [Magey TBC Critical Strike](https://github.com/magey/tbc-warrior/wiki/Attack-table#critical-strike)
[^29]: [Vanilla Wow Wiki Crushing Blow](https://vanilla-wow-archive.fandom.com/wiki/Crushing_blow)
"""

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Plots = "91a5bcdd-55d7-5caf-9e0b-520d859cae80"
Printf = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[compat]
Plots = "~1.18.1"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[Adapt]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "84918055d15b3114ede17ac6a7182f68870c16f7"
uuid = "79e6a3ab-5dfb-504d-930d-738a2a938a0e"
version = "3.3.1"

[[ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[Bzip2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "c3598e525718abcc440f69cc6d5f60dda0a1b61e"
uuid = "6e34b625-4abd-537c-b88f-471c36dfa7a0"
version = "1.0.6+5"

[[Cairo_jll]]
deps = ["Artifacts", "Bzip2_jll", "Fontconfig_jll", "FreeType2_jll", "Glib_jll", "JLLWrappers", "LZO_jll", "Libdl", "Pixman_jll", "Pkg", "Xorg_libXext_jll", "Xorg_libXrender_jll", "Zlib_jll", "libpng_jll"]
git-tree-sha1 = "e2f47f6d8337369411569fd45ae5753ca10394c6"
uuid = "83423d85-b0ee-5818-9007-b63ccbeb887a"
version = "1.16.0+6"

[[ColorSchemes]]
deps = ["ColorTypes", "Colors", "FixedPointNumbers", "Random", "StaticArrays"]
git-tree-sha1 = "ed268efe58512df8c7e224d2e170afd76dd6a417"
uuid = "35d6a980-a343-548e-a6ea-1d62b119f2f4"
version = "3.13.0"

[[ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[Colors]]
deps = ["ColorTypes", "FixedPointNumbers", "Reexport"]
git-tree-sha1 = "417b0ed7b8b838aa6ca0a87aadf1bb9eb111ce40"
uuid = "5ae59095-9a9b-59fe-a467-6f913c188581"
version = "0.12.8"

[[Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "dc7dedc2c2aa9faf59a55c622760a25cbefbe941"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.31.0"

[[CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[Contour]]
deps = ["StaticArrays"]
git-tree-sha1 = "9f02045d934dc030edad45944ea80dbd1f0ebea7"
uuid = "d38c429a-6771-53c6-b99e-75d170b6e991"
version = "0.5.7"

[[DataAPI]]
git-tree-sha1 = "ee400abb2298bd13bfc3df1c412ed228061a2385"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.7.0"

[[DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "4437b64df1e0adccc3e5d1adbc3ac741095e4677"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.9"

[[DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[EarCut_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "92d8f9f208637e8d2d28c664051a00569c01493d"
uuid = "5ae413db-bbd1-5e63-b57d-d24a61df00f5"
version = "2.1.5+1"

[[Expat_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "b3bfd02e98aedfa5cf885665493c5598c350cd2f"
uuid = "2e619515-83b5-522b-bb60-26c02a35a201"
version = "2.2.10+0"

[[FFMPEG]]
deps = ["FFMPEG_jll"]
git-tree-sha1 = "b57e3acbe22f8484b4b5ff66a7499717fe1a9cc8"
uuid = "c87230d0-a227-11e9-1b43-d7ebe4e7570a"
version = "0.4.1"

[[FFMPEG_jll]]
deps = ["Artifacts", "Bzip2_jll", "FreeType2_jll", "FriBidi_jll", "JLLWrappers", "LAME_jll", "LibVPX_jll", "Libdl", "Ogg_jll", "OpenSSL_jll", "Opus_jll", "Pkg", "Zlib_jll", "libass_jll", "libfdk_aac_jll", "libvorbis_jll", "x264_jll", "x265_jll"]
git-tree-sha1 = "3cc57ad0a213808473eafef4845a74766242e05f"
uuid = "b22a6f82-2f65-5046-a5b2-351ab43fb4e5"
version = "4.3.1+4"

[[FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[Fontconfig_jll]]
deps = ["Artifacts", "Bzip2_jll", "Expat_jll", "FreeType2_jll", "JLLWrappers", "Libdl", "Libuuid_jll", "Pkg", "Zlib_jll"]
git-tree-sha1 = "35895cf184ceaab11fd778b4590144034a167a2f"
uuid = "a3f928ae-7b40-5064-980b-68af3947d34b"
version = "2.13.1+14"

[[Formatting]]
deps = ["Printf"]
git-tree-sha1 = "8339d61043228fdd3eb658d86c926cb282ae72a8"
uuid = "59287772-0a20-5a39-b81b-1366585eb4c0"
version = "0.4.2"

[[FreeType2_jll]]
deps = ["Artifacts", "Bzip2_jll", "JLLWrappers", "Libdl", "Pkg", "Zlib_jll"]
git-tree-sha1 = "cbd58c9deb1d304f5a245a0b7eb841a2560cfec6"
uuid = "d7e528f0-a631-5988-bf34-fe36492bcfd7"
version = "2.10.1+5"

[[FriBidi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "aa31987c2ba8704e23c6c8ba8a4f769d5d7e4f91"
uuid = "559328eb-81f9-559d-9380-de523a88c83c"
version = "1.0.10+0"

[[GLFW_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libglvnd_jll", "Pkg", "Xorg_libXcursor_jll", "Xorg_libXi_jll", "Xorg_libXinerama_jll", "Xorg_libXrandr_jll"]
git-tree-sha1 = "dba1e8614e98949abfa60480b13653813d8f0157"
uuid = "0656b61e-2033-5cc2-a64a-77c0f6c09b89"
version = "3.3.5+0"

[[GR]]
deps = ["Base64", "DelimitedFiles", "GR_jll", "HTTP", "JSON", "Libdl", "LinearAlgebra", "Pkg", "Printf", "Random", "Serialization", "Sockets", "Test", "UUIDs"]
git-tree-sha1 = "b83e3125048a9c3158cbb7ca423790c7b1b57bea"
uuid = "28b8d3ca-fb5f-59d9-8090-bfdbd6d07a71"
version = "0.57.5"

[[GR_jll]]
deps = ["Artifacts", "Bzip2_jll", "Cairo_jll", "FFMPEG_jll", "Fontconfig_jll", "GLFW_jll", "JLLWrappers", "JpegTurbo_jll", "Libdl", "Libtiff_jll", "Pixman_jll", "Pkg", "Qt5Base_jll", "Zlib_jll", "libpng_jll"]
git-tree-sha1 = "e14907859a1d3aee73a019e7b3c98e9e7b8b5b3e"
uuid = "d2c73de3-f751-5644-a686-071e5b155ba9"
version = "0.57.3+0"

[[GeometryBasics]]
deps = ["EarCut_jll", "IterTools", "LinearAlgebra", "StaticArrays", "StructArrays", "Tables"]
git-tree-sha1 = "15ff9a14b9e1218958d3530cc288cf31465d9ae2"
uuid = "5c1252a2-5f33-56bf-86c9-59e7332b4326"
version = "0.3.13"

[[Gettext_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Libiconv_jll", "Pkg", "XML2_jll"]
git-tree-sha1 = "9b02998aba7bf074d14de89f9d37ca24a1a0b046"
uuid = "78b55507-aeef-58d4-861c-77aaff3498b1"
version = "0.21.0+0"

[[Glib_jll]]
deps = ["Artifacts", "Gettext_jll", "JLLWrappers", "Libdl", "Libffi_jll", "Libiconv_jll", "Libmount_jll", "PCRE_jll", "Pkg", "Zlib_jll"]
git-tree-sha1 = "47ce50b742921377301e15005c96e979574e130b"
uuid = "7746bdde-850d-59dc-9ae8-88ece973131d"
version = "2.68.1+0"

[[Grisu]]
git-tree-sha1 = "53bb909d1151e57e2484c3d1b53e19552b887fb2"
uuid = "42e2da0e-8278-4e71-bc24-59509adca0fe"
version = "1.0.2"

[[HTTP]]
deps = ["Base64", "Dates", "IniFile", "Logging", "MbedTLS", "NetworkOptions", "Sockets", "URIs"]
git-tree-sha1 = "c6a1fff2fd4b1da29d3dccaffb1e1001244d844e"
uuid = "cd3eb016-35fb-5094-929b-558a96fad6f3"
version = "0.9.12"

[[IniFile]]
deps = ["Test"]
git-tree-sha1 = "098e4d2c533924c921f9f9847274f2ad89e018b8"
uuid = "83e8ac13-25f8-5344-8a64-a9f2b223428f"
version = "0.5.0"

[[InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[IterTools]]
git-tree-sha1 = "05110a2ab1fc5f932622ffea2a003221f4782c18"
uuid = "c8e1da08-722c-5040-9ed9-7db0dc04731e"
version = "1.3.0"

[[IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "642a199af8b68253517b80bd3bfd17eb4e84df6e"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.3.0"

[[JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "81690084b6198a2e1da36fcfda16eeca9f9f24e4"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.1"

[[JpegTurbo_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "d735490ac75c5cb9f1b00d8b5509c11984dc6943"
uuid = "aacddb02-875f-59d6-b918-886e6ef4fbf8"
version = "2.1.0+0"

[[LAME_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "f6250b16881adf048549549fba48b1161acdac8c"
uuid = "c1c5ebd0-6772-5130-a774-d5fcae4a789d"
version = "3.100.1+0"

[[LZO_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "e5b909bcf985c5e2605737d2ce278ed791b89be6"
uuid = "dd4b983a-f0e5-5f8d-a1b7-129d4a5fb1ac"
version = "2.10.1+0"

[[LaTeXStrings]]
git-tree-sha1 = "c7f1c695e06c01b95a67f0cd1d34994f3e7db104"
uuid = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
version = "1.2.1"

[[Latexify]]
deps = ["Formatting", "InteractiveUtils", "LaTeXStrings", "MacroTools", "Markdown", "Printf", "Requires"]
git-tree-sha1 = "a4b12a1bd2ebade87891ab7e36fdbce582301a92"
uuid = "23fbe1c1-3f47-55db-b15f-69d7ec21a316"
version = "0.15.6"

[[LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[LibVPX_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "12ee7e23fa4d18361e7c2cde8f8337d4c3101bc7"
uuid = "dd192d2f-8180-539f-9fb4-cc70b1dcf69a"
version = "1.10.0+0"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[Libffi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "761a393aeccd6aa92ec3515e428c26bf99575b3b"
uuid = "e9f186c6-92d2-5b65-8a66-fee21dc1b490"
version = "3.2.2+0"

[[Libgcrypt_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libgpg_error_jll", "Pkg"]
git-tree-sha1 = "64613c82a59c120435c067c2b809fc61cf5166ae"
uuid = "d4300ac3-e22c-5743-9152-c294e39db1e4"
version = "1.8.7+0"

[[Libglvnd_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll", "Xorg_libXext_jll"]
git-tree-sha1 = "7739f837d6447403596a75d19ed01fd08d6f56bf"
uuid = "7e76a0d4-f3c7-5321-8279-8d96eeed0f29"
version = "1.3.0+3"

[[Libgpg_error_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "c333716e46366857753e273ce6a69ee0945a6db9"
uuid = "7add5ba3-2f88-524e-9cd5-f83b8a55f7b8"
version = "1.42.0+0"

[[Libiconv_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "42b62845d70a619f063a7da093d995ec8e15e778"
uuid = "94ce4f54-9a6c-5748-9c1c-f9c7231a4531"
version = "1.16.1+1"

[[Libmount_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "9c30530bf0effd46e15e0fdcf2b8636e78cbbd73"
uuid = "4b2f31a3-9ecc-558c-b454-b3730dcb73e9"
version = "2.35.0+0"

[[Libtiff_jll]]
deps = ["Artifacts", "JLLWrappers", "JpegTurbo_jll", "Libdl", "Pkg", "Zlib_jll", "Zstd_jll"]
git-tree-sha1 = "340e257aada13f95f98ee352d316c3bed37c8ab9"
uuid = "89763e89-9b03-5906-acba-b20f662cd828"
version = "4.3.0+0"

[[Libuuid_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "7f3efec06033682db852f8b3bc3c1d2b0a0ab066"
uuid = "38a345b3-de98-5d2b-a5d3-14cd9215e700"
version = "2.36.0+0"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "6a8a2a625ab0dea913aba95c11370589e0239ff0"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.6"

[[Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[MbedTLS]]
deps = ["Dates", "MbedTLS_jll", "Random", "Sockets"]
git-tree-sha1 = "1c38e51c3d08ef2278062ebceade0e46cefc96fe"
uuid = "739be429-bea8-5141-9913-cc70e7f3736d"
version = "1.0.3"

[[MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[Measures]]
git-tree-sha1 = "e498ddeee6f9fdb4551ce855a46f54dbd900245f"
uuid = "442fdcdd-2543-5da2-b0f3-8c86c306513e"
version = "0.3.1"

[[Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "4ea90bd5d3985ae1f9a908bd4500ae88921c5ce7"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.0"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[NaNMath]]
git-tree-sha1 = "bfe47e760d60b82b66b61d2d44128b62e3a369fb"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "0.3.5"

[[NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[Ogg_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "7937eda4681660b4d6aeeecc2f7e1c81c8ee4e2f"
uuid = "e7412a2a-1a6e-54c0-be00-318e2571c051"
version = "1.3.5+0"

[[OpenSSL_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "15003dcb7d8db3c6c857fda14891a539a8f2705a"
uuid = "458c3c95-2e84-50aa-8efc-19380b2a3a95"
version = "1.1.10+0"

[[Opus_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "51a08fb14ec28da2ec7a927c4337e4332c2a4720"
uuid = "91d4177d-7536-5919-b921-800302f37372"
version = "1.3.2+0"

[[OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[PCRE_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "b2a7af664e098055a7529ad1a900ded962bca488"
uuid = "2f80f16e-611a-54ab-bc61-aa92de5b98fc"
version = "8.44.0+0"

[[Parsers]]
deps = ["Dates"]
git-tree-sha1 = "c8abc88faa3f7a3950832ac5d6e690881590d6dc"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "1.1.0"

[[Pixman_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "b4f5d02549a10e20780a24fce72bea96b6329e29"
uuid = "30392449-352a-5448-841d-b1acce4e97dc"
version = "0.40.1+0"

[[Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[PlotThemes]]
deps = ["PlotUtils", "Requires", "Statistics"]
git-tree-sha1 = "a3a964ce9dc7898193536002a6dd892b1b5a6f1d"
uuid = "ccf2f8ad-2431-5c83-bf29-c5338b663b6a"
version = "2.0.1"

[[PlotUtils]]
deps = ["ColorSchemes", "Colors", "Dates", "Printf", "Random", "Reexport", "Statistics"]
git-tree-sha1 = "501c20a63a34ac1d015d5304da0e645f42d91c9f"
uuid = "995b91a9-d308-5afd-9ec6-746e21dbc043"
version = "1.0.11"

[[Plots]]
deps = ["Base64", "Contour", "Dates", "FFMPEG", "FixedPointNumbers", "GR", "GeometryBasics", "JSON", "Latexify", "LinearAlgebra", "Measures", "NaNMath", "PlotThemes", "PlotUtils", "Printf", "REPL", "Random", "RecipesBase", "RecipesPipeline", "Reexport", "Requires", "Scratch", "Showoff", "SparseArrays", "Statistics", "StatsBase", "UUIDs"]
git-tree-sha1 = "b93181645c1209d912d5632ba2d0094bc00703ad"
uuid = "91a5bcdd-55d7-5caf-9e0b-520d859cae80"
version = "1.18.1"

[[Preferences]]
deps = ["TOML"]
git-tree-sha1 = "00cfd92944ca9c760982747e9a1d0d5d86ab1e5a"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.2.2"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Qt5Base_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Fontconfig_jll", "Glib_jll", "JLLWrappers", "Libdl", "Libglvnd_jll", "OpenSSL_jll", "Pkg", "Xorg_libXext_jll", "Xorg_libxcb_jll", "Xorg_xcb_util_image_jll", "Xorg_xcb_util_keysyms_jll", "Xorg_xcb_util_renderutil_jll", "Xorg_xcb_util_wm_jll", "Zlib_jll", "xkbcommon_jll"]
git-tree-sha1 = "ad368663a5e20dbb8d6dc2fddeefe4dae0781ae8"
uuid = "ea2cea3b-5b76-57ae-a6ef-0a8af62496e1"
version = "5.15.3+0"

[[REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[RecipesBase]]
git-tree-sha1 = "b3fb709f3c97bfc6e948be68beeecb55a0b340ae"
uuid = "3cdcf5f2-1ef4-517c-9805-6587b60abb01"
version = "1.1.1"

[[RecipesPipeline]]
deps = ["Dates", "NaNMath", "PlotUtils", "RecipesBase"]
git-tree-sha1 = "2a7a2469ed5d94a98dea0e85c46fa653d76be0cd"
uuid = "01d81517-befc-4cb6-b9ec-a95719d0359c"
version = "0.3.4"

[[Reexport]]
git-tree-sha1 = "5f6c21241f0f655da3952fd60aa18477cf96c220"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.1.0"

[[Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "4036a3bd08ac7e968e27c203d45f5fff15020621"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.1.3"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Scratch]]
deps = ["Dates"]
git-tree-sha1 = "0b4b7f1393cff97c33891da2a0bf69c6ed241fda"
uuid = "6c6a2e73-6563-6170-7368-637461726353"
version = "1.1.0"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[Showoff]]
deps = ["Dates", "Grisu"]
git-tree-sha1 = "91eddf657aca81df9ae6ceb20b959ae5653ad1de"
uuid = "992d4aef-0814-514b-bc4d-f2e9a6c4116f"
version = "1.0.3"

[[Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "a43a7b58a6e7dc933b2fa2e0ca653ccf8bb8fd0e"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.2.6"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[StatsAPI]]
git-tree-sha1 = "1958272568dc176a1d881acb797beb909c785510"
uuid = "82ae8749-77ed-4fe6-ae5f-f523153014b0"
version = "1.0.0"

[[StatsBase]]
deps = ["DataAPI", "DataStructures", "LinearAlgebra", "Missings", "Printf", "Random", "SortingAlgorithms", "SparseArrays", "Statistics", "StatsAPI"]
git-tree-sha1 = "2f6792d523d7448bbe2fec99eca9218f06cc746d"
uuid = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
version = "0.33.8"

[[StructArrays]]
deps = ["Adapt", "DataAPI", "StaticArrays", "Tables"]
git-tree-sha1 = "000e168f5cc9aded17b6999a560b7c11dda69095"
uuid = "09ab397b-f2b6-538f-b94a-2f83cf4a842a"
version = "0.6.0"

[[TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "TableTraits", "Test"]
git-tree-sha1 = "8ed4a3ea724dac32670b062be3ef1c1de6773ae8"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.4.4"

[[Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[Wayland_jll]]
deps = ["Artifacts", "Expat_jll", "JLLWrappers", "Libdl", "Libffi_jll", "Pkg", "XML2_jll"]
git-tree-sha1 = "3e61f0b86f90dacb0bc0e73a0c5a83f6a8636e23"
uuid = "a2964d1f-97da-50d4-b82a-358c7fce9d89"
version = "1.19.0+0"

[[Wayland_protocols_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Wayland_jll"]
git-tree-sha1 = "2839f1c1296940218e35df0bbb220f2a79686670"
uuid = "2381bf8a-dfd0-557d-9999-79630e7b1b91"
version = "1.18.0+4"

[[XML2_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libiconv_jll", "Pkg", "Zlib_jll"]
git-tree-sha1 = "1acf5bdf07aa0907e0a37d3718bb88d4b687b74a"
uuid = "02c8fc9c-b97f-50b9-bbe4-9be30ff0a78a"
version = "2.9.12+0"

[[XSLT_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Libgcrypt_jll", "Libgpg_error_jll", "Libiconv_jll", "Pkg", "XML2_jll", "Zlib_jll"]
git-tree-sha1 = "91844873c4085240b95e795f692c4cec4d805f8a"
uuid = "aed1982a-8fda-507f-9586-7b0439959a61"
version = "1.1.34+0"

[[Xorg_libX11_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libxcb_jll", "Xorg_xtrans_jll"]
git-tree-sha1 = "5be649d550f3f4b95308bf0183b82e2582876527"
uuid = "4f6342f7-b3d2-589e-9d20-edeb45f2b2bc"
version = "1.6.9+4"

[[Xorg_libXau_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "4e490d5c960c314f33885790ed410ff3a94ce67e"
uuid = "0c0b7dd1-d40b-584c-a123-a41640f87eec"
version = "1.0.9+4"

[[Xorg_libXcursor_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXfixes_jll", "Xorg_libXrender_jll"]
git-tree-sha1 = "12e0eb3bc634fa2080c1c37fccf56f7c22989afd"
uuid = "935fb764-8cf2-53bf-bb30-45bb1f8bf724"
version = "1.2.0+4"

[[Xorg_libXdmcp_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "4fe47bd2247248125c428978740e18a681372dd4"
uuid = "a3789734-cfe1-5b06-b2d0-1dd0d9d62d05"
version = "1.1.3+4"

[[Xorg_libXext_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "b7c0aa8c376b31e4852b360222848637f481f8c3"
uuid = "1082639a-0dae-5f34-9b06-72781eeb8cb3"
version = "1.3.4+4"

[[Xorg_libXfixes_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "0e0dc7431e7a0587559f9294aeec269471c991a4"
uuid = "d091e8ba-531a-589c-9de9-94069b037ed8"
version = "5.0.3+4"

[[Xorg_libXi_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll", "Xorg_libXfixes_jll"]
git-tree-sha1 = "89b52bc2160aadc84d707093930ef0bffa641246"
uuid = "a51aa0fd-4e3c-5386-b890-e753decda492"
version = "1.7.10+4"

[[Xorg_libXinerama_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll"]
git-tree-sha1 = "26be8b1c342929259317d8b9f7b53bf2bb73b123"
uuid = "d1454406-59df-5ea1-beac-c340f2130bc3"
version = "1.1.4+4"

[[Xorg_libXrandr_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libXext_jll", "Xorg_libXrender_jll"]
git-tree-sha1 = "34cea83cb726fb58f325887bf0612c6b3fb17631"
uuid = "ec84b674-ba8e-5d96-8ba1-2a689ba10484"
version = "1.5.2+4"

[[Xorg_libXrender_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "19560f30fd49f4d4efbe7002a1037f8c43d43b96"
uuid = "ea2f1a96-1ddc-540d-b46f-429655e07cfa"
version = "0.9.10+4"

[[Xorg_libpthread_stubs_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "6783737e45d3c59a4a4c4091f5f88cdcf0908cbb"
uuid = "14d82f49-176c-5ed1-bb49-ad3f5cbd8c74"
version = "0.1.0+3"

[[Xorg_libxcb_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "XSLT_jll", "Xorg_libXau_jll", "Xorg_libXdmcp_jll", "Xorg_libpthread_stubs_jll"]
git-tree-sha1 = "daf17f441228e7a3833846cd048892861cff16d6"
uuid = "c7cfdc94-dc32-55de-ac96-5a1b8d977c5b"
version = "1.13.0+3"

[[Xorg_libxkbfile_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libX11_jll"]
git-tree-sha1 = "926af861744212db0eb001d9e40b5d16292080b2"
uuid = "cc61e674-0454-545c-8b26-ed2c68acab7a"
version = "1.1.0+4"

[[Xorg_xcb_util_image_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "0fab0a40349ba1cba2c1da699243396ff8e94b97"
uuid = "12413925-8142-5f55-bb0e-6d7ca50bb09b"
version = "0.4.0+1"

[[Xorg_xcb_util_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libxcb_jll"]
git-tree-sha1 = "e7fd7b2881fa2eaa72717420894d3938177862d1"
uuid = "2def613f-5ad1-5310-b15b-b15d46f528f5"
version = "0.4.0+1"

[[Xorg_xcb_util_keysyms_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "d1151e2c45a544f32441a567d1690e701ec89b00"
uuid = "975044d2-76e6-5fbe-bf08-97ce7c6574c7"
version = "0.4.0+1"

[[Xorg_xcb_util_renderutil_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "dfd7a8f38d4613b6a575253b3174dd991ca6183e"
uuid = "0d47668e-0667-5a69-a72c-f761630bfb7e"
version = "0.3.9+1"

[[Xorg_xcb_util_wm_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xcb_util_jll"]
git-tree-sha1 = "e78d10aab01a4a154142c5006ed44fd9e8e31b67"
uuid = "c22f9ab0-d5fe-5066-847c-f4bb1cd4e361"
version = "0.4.1+1"

[[Xorg_xkbcomp_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_libxkbfile_jll"]
git-tree-sha1 = "4bcbf660f6c2e714f87e960a171b119d06ee163b"
uuid = "35661453-b289-5fab-8a00-3d9160c6a3a4"
version = "1.4.2+4"

[[Xorg_xkeyboard_config_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Xorg_xkbcomp_jll"]
git-tree-sha1 = "5c8424f8a67c3f2209646d4425f3d415fee5931d"
uuid = "33bec58e-1273-512f-9401-5d533626f822"
version = "2.27.0+4"

[[Xorg_xtrans_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "79c31e7844f6ecf779705fbc12146eb190b7d845"
uuid = "c5fb5394-a638-5e4d-96e5-b29de1b5cf10"
version = "1.4.0+3"

[[Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[Zstd_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "cc4bf3fdde8b7e3e9fa0351bdeedba1cf3b7f6e6"
uuid = "3161d3a3-bdf6-5164-811a-617609db77b4"
version = "1.5.0+0"

[[libass_jll]]
deps = ["Artifacts", "Bzip2_jll", "FreeType2_jll", "FriBidi_jll", "JLLWrappers", "Libdl", "Pkg", "Zlib_jll"]
git-tree-sha1 = "acc685bcf777b2202a904cdcb49ad34c2fa1880c"
uuid = "0ac62f75-1d6f-5e53-bd7c-93b484bb37c0"
version = "0.14.0+4"

[[libfdk_aac_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "7a5780a0d9c6864184b3a2eeeb833a0c871f00ab"
uuid = "f638f0a6-7fb0-5443-88ba-1cc74229b280"
version = "0.1.6+4"

[[libpng_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Zlib_jll"]
git-tree-sha1 = "94d180a6d2b5e55e447e2d27a29ed04fe79eb30c"
uuid = "b53b4c65-9356-5827-b1ea-8c7a1a84506f"
version = "1.6.38+0"

[[libvorbis_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Ogg_jll", "Pkg"]
git-tree-sha1 = "c45f4e40e7aafe9d086379e5578947ec8b95a8fb"
uuid = "f27f6e37-5d2b-51aa-960f-b287f2bc3b7a"
version = "1.3.7+0"

[[nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"

[[x264_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "d713c1ce4deac133e3334ee12f4adff07f81778f"
uuid = "1270edf5-f2f9-52d2-97e9-ab00b5d0237a"
version = "2020.7.14+2"

[[x265_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "487da2f8f2f0c8ee0e83f39d13037d6bbf0a45ab"
uuid = "dfaa095f-4041-5dcd-9319-2fabd8486b76"
version = "3.0.0+3"

[[xkbcommon_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg", "Wayland_jll", "Wayland_protocols_jll", "Xorg_libxcb_jll", "Xorg_xkeyboard_config_jll"]
git-tree-sha1 = "ece2350174195bb31de1a63bea3a41ae1aa593b6"
uuid = "d8fb68d0-12a3-5cfd-a85a-d49703b185fd"
version = "0.9.1+5"
"""

# ╔═╡ Cell order:
# ╟─0f52f8be-4ca8-11eb-0361-2f44a5fd6e21
# ╟─bc9dba04-42e4-11eb-3356-b7f7e7f2b09b
# ╟─d6f023fa-42e0-11eb-328b-9da41d7ea10e
# ╟─c1337ff4-52e3-11eb-108d-bb9834752d68
# ╟─c9fc6ee4-4c8e-11eb-3bc3-0353c0d0f086
# ╟─118d4294-4c92-11eb-05db-87f2be2c7f12
# ╟─128ab02a-4c93-11eb-27cf-159b3b544034
# ╟─307f1f80-4c95-11eb-13d6-bf2a802e5af1
# ╟─87a33612-52e8-11eb-2e1c-7786439c8b65
# ╟─241a2098-433d-11eb-0206-3929dacd9d11
# ╟─b9aabf0a-52e2-11eb-1294-71e5ba92375c
# ╟─d765a9fc-52d7-11eb-27eb-03a2322c841b
# ╟─f5529f3c-52d8-11eb-1af7-236ecc764460
# ╟─4463e0ac-52e0-11eb-39a3-6fa91893272f
# ╟─eca6341a-52e3-11eb-0f4e-e79ff972f5c0
# ╟─c7b4edf4-59d7-11eb-35cd-e5f991cd4571
# ╟─37dfaf28-52e5-11eb-2614-8725f5ba4bbb
# ╟─bdb8e66e-579f-11eb-1461-a17d261169a5
# ╟─45b83ac4-64e3-11eb-02e3-ffddc00ff07b
# ╟─80f48a1a-553d-11eb-1913-1d1dce727bd0
# ╟─0e6409ca-553e-11eb-0f81-2301274e5ad9
# ╟─eeee1aca-553f-11eb-0972-23722e09b627
# ╟─cc6d38ac-4239-11eb-1091-cf1f1fe191e5
# ╟─6e2dfa2e-4239-11eb-3386-15e88e1eec08
# ╟─a9c7cc84-5bf4-11eb-31fa-b93360c96fbf
# ╟─d096422a-5300-11eb-3be2-07a012305a34
# ╟─3d4093e0-4cb6-11eb-38a1-e51d199defdf
# ╟─6b217a20-42ef-11eb-2068-21a3b2fdec3e
# ╟─c93ebdd0-4ca8-11eb-00ce-03acafd0d177
# ╟─38e99776-4228-11eb-33b6-0b0d9a13879a
# ╟─b4009394-432a-11eb-05ea-397eecfc9d15
# ╟─9e9edbc0-4ca6-11eb-1fba-9d4e2c613d91
# ╟─c048fdda-42f3-11eb-1ae6-b3e2f74aa3ed
# ╟─764c10aa-4cad-11eb-19c2-592a64a7d875
# ╟─8ba85768-4caf-11eb-3b46-079d676838e7
# ╟─f14a269c-4cb3-11eb-379b-6952f9cd0650
# ╟─d8f4788e-42dd-11eb-0e86-f97e7de17923
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
